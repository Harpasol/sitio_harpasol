+++
title = "tmp"
date = 2019-09-19T08:50:05-05:00
draft = false
tags = ["soporte-personas","tox"]
+++

[tmp@toxme.io](https://toxme.io/u/tmp) - https://toxme.io/  (Soporte en [Parabola][p], [Trisquel][t], [Guix][g], [PureOS][ps])

[Usa ToxChat](https://tox.chat)

[p]: /conceptos/parabola
[t]: /conceptos/trisquel
[g]: /conceptos/guix
[ps]: /conceptos/pureos
