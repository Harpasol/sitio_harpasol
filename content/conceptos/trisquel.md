+++
title = "Trisquel GNU/Linux"
date = 2019-09-04T08:50:05-05:00
draft = false
tags = ["distribuciones", "trisquel", "ubuntu", "gnu"]
+++
[Trisquel][t] es un sistema operativo completamente libre basado en [GNU/Linux][g]. Trisquel deriva de Ubuntu pero sólo incluye [software libre][sl]; es libre en el sentido de libertad. Los usuarios pueden ejecutar, copiar, distribuir, estudiar, cambiar y mejorar Trisquel (o alguna parte del mismo). Estas libertades apoyan la autonomía y las libertades de los usuarios. Trisquel viene listo para ser utilizado en el hogar y en el trabajo. Es [fácil][f] encontrar e instalar software más especializado. Explore sus opciones en nuestra [documentación][d] o pida a ayuda nuestra [comunidad][c].

Las funciones de accesibilidad son una parte importante de la edición estándar de Trisquel. Nuestros manuales son amigables con los lectores de pantalla; también ofrecemos una serie de útiles guías de audio.

Trisquel es gratis; no cobramos dinero para el uso de este sistema operativo. Si desea apoyar económicamente a Trisquel, puede comprar hardware de [ThinkPenguin][tp], o hacer una [donación][do] directamente a nosotros.


[t]: https://trisquel.info/es
[g]: https://www.gnu.org/gnu/gnu-linux-faq.es.html#why
[sl]: http://www.gnu.org/philosophy/free-sw.html
[f]: https://trisquel.info/es/wiki/instalar-actualizar-y-remover-software
[d]: https://trisquel.info/es/wiki/todos-los-manuales
[c]: https://trisquel.info/es/wiki/todos-los-manuales
[tp]: https://trisquel.info/es/wiki/thinkpenguino
[do]: https://trisquel.info/es/donate

