+++
title = "PureOS"
date = 2019-09-04T08:50:05-05:00
draft = false
tags = ["distribuciones", "pureos", "debian", "gnu"]
+++
### A GNU/Linux operating system.
 [PureOS][p] is a fully [free/libre and open source][sl] [GNU/Linux][g] operating system, [endorsed][e] by the [Free Software Foundation][fsf], and based on another GNU/Linux OS called [Debian][d]. Check also what [GNU][gnu] means.

[p]: https://pureos.net/
[g]: https://en.wikipedia.org/wiki/GNU_Linux
[sl]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[e]: http://www.gnu.org/distros/free-distros.html
[fsf]: https://www.fsf.org/
[d]: https://www.debian.org/
[gnu]: http://www.gnu.org/


